/*
�������-������� 
��� �������� �����: prefix.in
��� ��������� �����: prefix.out
����������� �� �������: 2 �������
����������� �� ������: 256 ��������
��������� �������-������� ��� �������� ������ s.
������ �������� �����
������ ������ �������� ����� �������� s (1 <= |s| <= 10^6). ������ ������� �� ���� ���������� ��������.
������ ��������� �����
�������� �������� �������-������� ������ s ��� ���� �������� 1, 2, . . . , |s|.
������ 1
prefix.in 
aaaAAA 
prefix.out
0 1 2 0 0 0
������ 2
prefix.in 
abacaba 
prefix.out
0 0 1 0 1 2 3
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
 
using namespace std;
 
vector <int> prefix(string s){
    int n = s.length();
	vector <int> ans;
	ans.resize(n);
	ans[0] = 0;
  for (int i = 1 ; i<n ; i++){
      int k = ans[i - 1];
      while ((k > 0) && (s[i] != s[k])){
          k = ans[k - 1];
      }
      if (s[i] == s[k]){
          k++;
	   }
      ans[i] = k;
	}
  return ans ;
}
 
int main(){
	
    ifstream in ("prefix.in");
	ofstream out ("prefix.out");
	
	string s;
	
	in >> s;
	
	int n = s.length();

	vector <int> ans;
	ans.resize(n);
	ans[0] = 0;
	
	ans = prefix(s);
	
	for (int i=0 ; i<n ; i++){
		out << ans[i] << " ";
	}	
}
