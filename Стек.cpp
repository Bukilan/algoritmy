/*����
��� �������� �����: stack.in
��� ��������� �����: stack.out
���������� ���� � ��������� ������ � ��������
������ �������� ����� 
� ������ ������ ����������� ���������� ������� M (1 <= M <= 10^6) ������ ����. ������� �������� ����� ���� ������� 
������ ��������� ����� 
�� ����� ������� ��������� ������ ��������� ���������
stack.in 
6
+ 1
+ 10
-
+ 2
+ 1234
-
stack.out
10
1234
*/
#include <iostream>
#include <fstream>

using namespace std;

class Stack {
private:

    struct Info {
        int x;
        Info *prev = NULL;
    };
    Info *head;

public:
    Stack() {
        head = NULL;
    }
    void push(int x);
    int top();
    bool isEmpty();
    void pop();
    ~Stack()
    {
        while(!isEmpty())
        {
            Info *p = head;
            head = head -> prev;
            delete p;
        }
    }
};

void Stack:: push(int x)
{
    Info *p = new Info;
    p -> x = x;
    p -> prev = head;
    head = p;
}

int Stack:: top()
{
    return head -> x;
}

bool Stack:: isEmpty()
{
    return head ? false : true;
}

void Stack:: pop()
{
    if(!isEmpty())
    {
        Info *pv = head;
        head = head -> prev;
        delete pv;
    }
}

int str_to_int(string str , int k)
{
    int res = 0;
    for(int i = k ; i < str.length() ; i++)
    {
        res = res * 10 + str[i] - '0';
    }
    return res;
}

int main()
{
    ifstream cin ("stack.in");
    ofstream cout ("stack.out");

    Stack A;
    int n;
    cin >> n;
    char c;
    long long x;
    for(int i = 0 ; i < n ; i++)
    {
        cin >> c;
        if(c == '+')
        {
            cin >> x;
            A.push(x);
        }
        if(c == '-')
        {
            cout << A.top() << endl;
            A.pop();
        }
    }

    return 0;
}
