/*
��������� ������� ����, ��� ������� QuickSort �������� �� ������ �����
������ �������� �����
� ������ ������ ��������� ������������ ����� n (1 <= n <= 70000).
������ ��������� �����
������� ������������ ����� �� 1 �� n, �� ������� ������� ���������� �������� ������������
����� ���������. ���� ����� ������������ ���������, ������� ����� �� ���.
������
antiqs.in 
3 
antiqs.out
1 3 2
*/

#include <iostream> 
#include <fstream> 
using namespace std; 


int main() 
{ 
ifstream fin ("antiqs.in"); 
ofstream fout ("antiqs.out"); 

int n; 
fin >> n; 
int arr[n]; 

for (int i = 0; i < n; ++i) 
arr[i] = i + 1; 

int swap; 
for (int j = 0; j < n; ++j) 
{ 
swap = arr[j]; 
arr[j] = arr[j/2]; 
arr[j/2] = swap; 
} 

for (int k = 0; k < n ; ++k) 
{ 
fout << arr[k] << " "; 
} 

return 0; 

}
