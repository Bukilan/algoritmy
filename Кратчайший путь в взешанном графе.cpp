/*
���������� ���� 
��� �������� �����: pathmgep.in
��� ��������� �����: pathmgep.out
����������� �� �������: 2 �������
����������� �� ������: 64 ���������
��� ��������������� ���������� ����. ������� ���������� ���������� �� ����� ��������
������� �� ������.
������ �������� �����
� ������ ������ �������� ����� ��� �����: N, S � F (1 <= N <= 2000, 1 <= S, F <= N), ��� N � ���������� ������ �����, S � ��������� �������, � F � ��������.
� ��������� N ������� �� N ����� � ������� ��������� �����, ���  -1 �������� ���������� ����� ����� ���������, � �����
��������������� ����� � ����������� ����� ������� ����. �� ������� ��������� ������� ������ ����.
������ ��������� �����
������� ������� ���������� ��� ?1, ���� ���� ����� ���������� ��������� �� ����������.
������
pathmgep.in 
3 1 2
0 -1 2
3 0 -1
-1 4 0
pathmgep.out
6
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <climits>
 
using namespace std;
 
const long long inf = LLONG_MAX;
 
int main()
{
	
	ifstream in ("pathmgep.in");
    ofstream out ("pathmgep.out");
    
    int n, from , to;
    in >> n >> from >> to;
	int weight; 
	
    vector < pair<int , int> > graph[n];
    
    long long distance[n];
    bool used[n];
    
    for(int i = 0 ; i < n ; i ++)
    {
        used[i] = false;
		distance[i] = inf;
        for(int j = 0 ; j < n ; j++)
        {
            in >> weight;
            if(weight != -1)
            {
                graph[i].push_back(make_pair(j , weight));
            }
        }
    }
    distance[from - 1] = 0;
    for(int i = 0 ; i < n ; i++)
    {
        int v = -1;
        for(int j = 0 ; j < n ; j++)
        {
            if(!used[j] && (v == -1 || distance[j] < distance[v]))
            {
                v = j;
            }
        }
        if(distance[v] == inf)
        {
            break;
        }
        used[v] = true;
        for(int j = 0 ; j < graph[v].size() ; j++)
        {
            int goal = graph[v][j].first , len = graph[v][j].second;
            if(distance[v] + len < distance[goal])
            {
                distance[goal] = distance[v] + len;
            }
        }
    }
     
    out << (distance[to - 1] == inf ? -1 : distance[to - 1]);

    return 0;
}
