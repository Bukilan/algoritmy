/*
���������� ���� �� ������ ������� �� ������
��� �������� �����: pathsg.in
��� ��������� �����: pathsg.out
����������� �� �������: 2 �������
����������� �� ������: 64 ���������
����� ��������������� ���������� ������� ����. ������� ������� ���������� ����� ���
���������.
������ �������� �����
������ ������ �������� ����� �������� ����� n � m � ���������� ������ � ����� � �����
�������������� (1 <= n <= 200, 0 <= m <= 10 000). ��������� m ����� �������� �� ��� ����� �
�������, ������� ��������� ��������������� ����� ����� � ��� ���. ���� ����� �������������� �
�� ��������� 10^4
������ ��������� �����
�������� � �������� ���� n ����� �� n ����� � ��� ������ ���� ������ �������� ����������
����� ����.
�������
pathsg.in 
3 3
1 2 5
2 3 2
3 1 8
pathsg.out
0 5 7
10 0 2
8 13 0
*/

#include <iostream>
#include <fstream>
#include <climits>
 
using namespace std;
 
const long long inf = LLONG_MAX;
 
int main()
{
    ifstream in ("pathsg.in");
    ofstream out ("pathsg.out");

    int n , m;
    in >> n >> m;
    
    long long graph[n][n];
    int from , to , weight;
    
    for(int i = 0 ; i < n ; i++){
        for(int j = 0 ; j < n ; j++){
            if (i == j){
            	graph[i][j] = 0;
			}
			else{
				graph[i][j] = inf;
			}
        }
    }
    
    for(int i = 0 ; i < m ; i++){
        in >> from >> to >> weight;
        graph[from - 1][to - 1] = weight;
    }
    
    for(int i = 0 ; i < n ; i++){
        for(int j = 0 ; j < n ; j++){
            for(int k = 0 ; k < n ; k++){
                if(graph[j][i] != inf && graph[i][k] != inf)
                {
                    graph[j][k] = min(graph[j][k] , graph[j][i] + graph[i][k]);
                }
            }
        }
    }
    
    for(int i = 0 ; i < n ; i++){
        for(int j = 0 ; j < n ; j++){
        	out << graph[i][j] << " ";
        }
        out << endl;
    }
    
    return 0;
}
