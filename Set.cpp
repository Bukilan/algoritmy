/*Set 
��� �������� �����: set.in
��� ��������� �����: set.out
����� ����������� ��������� � �������������� ��� ������� ��� ������������� ��������� (set, map, LinkedHashMap ���)
����� �������� �����
� insert x � ��������� ������� � � ���������. ���� ������� ��� ���� � ���������, �� ������ ������ �� ����
� delete x � ������� ������� �. ���� � ���, ������ ������ �� ����
� exists x ����� ���� � ���� � ��������� �������� �true�, ���� ��� �false�.
����� ��������� �����
������� �� ������ ���������� ������� exist �� �������
set.in 
insert 2
insert 5
insert 3
exists 2
exists 4
insert 2
delete 2
exists 2
set.out
true
false
false
*/

#include <fstream>
#include <iostream>
#include <cmath>

using namespace std;

struct Node {
    string key;
    string value;
    Node *next;
};

class List {
private:
    Node *Head;

public:
    List(){Head = NULL;}
    //~List();
    void L_add (string x, string y);
    void L_remove(string x);
    Node *L_exist(string x);
};

/*List::~List(){
	while (Head != NULL)
	Node *temp = Head -> next;
	delete Head;
	Head = temp;
}*/

void List::L_add(string x, string y)
{

    Node *temp = L_exist(x);

    if (temp != NULL) {
        temp->value = y;
    }
    else{
        temp = new Node();
        temp->key = x;
        temp->value = y;
        temp->next = Head;
        Head = temp;
    }
}

void List::L_remove(string x){

    if (Head == NULL)
        return;

    if (Head -> key == x ){
        Node *temp = Head;
        Head = Head -> next;
        delete temp;
    }
    else {
        Node *temp = Head;
        while (temp -> next != NULL) {
            if (temp -> next -> key == x){
                Node *vrem = temp -> next;
                temp -> next = temp -> next -> next;
                delete vrem;
                break;
            }
            temp = temp->next;
        }
    }
};

Node *List::L_exist(string x){

    Node *temp = Head;
    while (temp != NULL){
        if (temp -> key == x){
            return temp;
        }
        temp = temp -> next;
    }

    return NULL;
}


int Hash(string str) {
    int k = 1 ;
    int index = 0;
    for(int i = 0 ; i < str.length() ; i++) {
        index += str[i] * k;
        k += 26;
    }
    return abs(index & 65535);

};

void insert(string x , string y, List mass[]){
    int hash = Hash(x);
    mass[hash].L_add(x,y);
}

void remove(string x, List mass[]){
    int hash = Hash(x);
    mass[hash].L_remove(x);
}

string exist(string x, List mass[]){
    int hash = Hash(x);
    Node *temp = mass[hash].L_exist(x);
    if (temp != NULL){
        return temp->value;
    }
    else{
        return "none";
    }
}

int main (){

    ifstream in ("map.in");
    ofstream out ("map.out");

    List hash[65535];
    string s;

    while (in >> s){
        if (s == "put"){
            string y, x;
            in >> x >> y;
            insert(x ,y, hash);
        }

        if (s == "delete"){
            string x;
            in >> x;
            remove(x, hash);
        }

        if (s == "get"){
            string x;
            in >> x;
            out << exist(x, hash) << "\n";
        }
    }
}
