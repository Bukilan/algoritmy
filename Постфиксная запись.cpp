/*����������� ������ 
��� �������� �����: postfix.in
��� ��������� �����: postfix.out
����������� �� �������: 2 �������
����������� �� ������: 64 ���������
� ����������� ������ (��� �������� �������� ������) �������� ������������ ����� ���� ���������.
��������, ����� ���� ����� A � B ������������ ��� A B +. ������ B C + D ? ����������
��������� ��� (B+C)?D, � ������ A B C + D ? + �������� A+(B+C)?D. ����������� �����������
������ � ���, ��� ��� �� ������� ������ � �������������� ���������� � ���������� ����������
��� ������ ������.
���� ��������� � �������� �������� ������. ���������� ��� ��������.
���������: ����������� ����.
������ �������� �����
� ������������ ������ �������� ��������� � ����������� ������, ���������� �����������
����� � �������� +, ?, ?. ������ �������� �� ����� 100 ����� � ��������.
������ ��������� �����
���������� ������� �������� ����������� ���������. �������������, ��� ��������� ���������,
� ����� ���������� ���� ������������� ���������� �� ������ ������ 2
31
.
������
postfix.in
8 9 + 1 7 - * 
postfix.out
-102
*/
#include <iostream>
#include <fstream>

using namespace std;

class Stack{
private:
    struct Info{
        int x;
        Info *prev = NULL;
    };
    Info *head;
public:
    Stack(){
        head = NULL;
    }
    void push(int x);
    int top();
    bool isEmpty();
    void pop();
    ~Stack(){
        while(!isEmpty()){
            Info *p = head;
            head = head -> prev;
            delete p;
        }
    }
};

void Stack:: push(int x){
    Info *p = new Info;
    p -> x = x;
    p -> prev = head;
    head = p;
}

int Stack:: top(){
    return head -> x;
}

bool Stack:: isEmpty(){
    return head ? false : true;
}

void Stack:: pop(){
    if(!isEmpty()){
        Info *pv = head;
        head = head -> prev;
        delete pv;
    }
}

int main(){
    ifstream cin ("postfix.in");
    ofstream cout ("postfix.out");

    Stack A;
    char c;
    int x;
    while(cin >> c){
        if(c != ' '){
            if(c >= '0' && c <= '9'){
                A.push(c - '0');
            }
            else{
                if(c == '-'){
                    x = A.top();
                    A.pop();
                    x = A.top() - x;
                    A.pop();
                    A.push(x);
                }
                if(c == '+'){
                    x = A.top();
                    A.pop();
                    x = A.top() + x;
                    A.pop();
                    A.push(x);
                }
                if(c == '*'){
                    x = A.top();
                    A.pop();
                    x = A.top() * x;
                    A.pop();
                    A.push(x);
                }
            }
        }
    }
 	cout << A.top();
    return 0;
}
