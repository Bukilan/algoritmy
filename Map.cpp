/*Map 
��� �������� �����: map.in
��� ��������� �����: map.out
����� ����������� ������������� ������ � ������������� ��� �������. ������ ������������ ���������� (set, map, LinkedHashMap ���)
������� ����
� put x y � ��������� � ������������ ����� � �������� y. ���� ���� ����, ���� �������� 
� delete x � ������� ���� x. ���� � ���, ������ ������ �� ���� 
� get x � ���� � ���� � ������������� �������, �� �������� ��������������� ��������, ���� ���, �� ��������  �none�.
�������� ����
�� ������� ������� ���������� �������� get
map.in
put hello privet
put bye poka
get hello
get bye
delete hello
get hello 
map.out
privet
poka
none
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>

using namespace std;


int hash(string str) {
    int k = 1 ;
    int index = 0;
    for(int i = 0 ; i < str.length() ; i++) {
        index += str[i] * k;
        k += 26;
    }
    return abs(index & 65535);
}

int main() {
    ifstream in ("map.in");
    ofstream out ("map.out");

    string str;
    vector < pair<string , string> > map[65536];

    int index;
    while(in >> str) {
        if(str == "put") {
            string key , value;
            in >> key >> value;
            index = hash(key);
            int k = 0;
            for(int i = 0 ; i < map[index].size() ; i++) {
                if(map[index][i].first == key) {
                    k = 1;
                    map[index][i].second = value;
                    break;
                }
            }
            if(k == 0) {
                map[index].push_back(make_pair(key , value));
            }
        }
        if(str == "delete") {
            string key , value;
            in >> key;
            index = hash(key);
            for(int i = 0 ; i < map[index].size() ; i++) {
                if(map[index][i].first == key) {
                    map[index].erase(map[index].begin() + i);
                }
            }
        }
        if(str == "get")
        {
            string key , value;
            in >> key;
            index = hash(key);
            int k = 0;
            for(int i = 0 ; i < map[index].size() ; i++)
            {
                if(map[index][i].first == key)
                {
                    k = 1;
                    out << map[index][i].second << endl;
                    break;
                }
            }
            if(k == 0)
            {
                out << "none" << endl;
            }
        }
    }
    return 0;
}
