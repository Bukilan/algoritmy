/*
������� ����� ��������� � ������ 
��� �������� �����: search2.in
��� ��������� �����: search2.out
����������� �� �������: 2 �������
����������� �� ������: 256 ��������
���� ������ p � t. ��������� ����� ��� ��������� ������ p � ������ t � �������� ���������.
������ �������� �����
������ ������ �������� ����� �������� p, ������ � t (1 <= |p|, |t| <= 10^6). ������ ������� �� ���� ���������� ��������.
������ ��������� �����
� ������ ������ �������� ���������� ��������� ������ p � ������ t. 
�� ������ ������ �������� � ������������ ������� ������ �������� ������ t, � ������� ���������� ��������� p.
����������������� � �������.
�������
search2.in 
aba
abaCaba
search2.out
2
1 5
*/

#include <string>
#include <vector>
#include <iostream>
#include <fstream>

using namespace std;

vector <int> KMP (string s, int m){
	int n = s.length();
	vector <int> ans;
	vector <int> b;
	b.resize(n);
	b[0] = 0;
  for (int i = 1 ; i<n ; i++){
      int k = b[i - 1];
      while ((k > 0) && (s[i] != s[k])){
          k = b[k - 1];
      }
      if (s[i] == s[k]){
          k++;
	   }
      b[i] = k;
      if (b[i] == m){
      	ans.push_back(i);
	  }
	}
  return ans ;
}

int main(){
	
	ifstream in ("search2.in");
	ofstream out ("search2.out");
	
	string t;
	string p;
	
	in >> t >> p;

	int m = t.length();
	
	vector < int > ans;
	
	string PrefString = t + "#" + p;
	
	ans = KMP(PrefString,m);
	
	out << ans.size() << endl;
	
	for (int i=0 ; i< ans.size() ; i++){
			out << (ans[i] - 2*m + 1) << " ";
	}
}


