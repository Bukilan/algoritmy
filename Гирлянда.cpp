/*
�������� 
��� �������� �����: garland.in
��� ��������� �����: garland.out
����������� �� �������: 2 �������
����������� �� ������: 256 ��������
�������� ������� �� n �������� �� ����� �������. ���� � ����� �������� �� �������� ������
A �� (h[1] = A). ��������� ���� ������� �������� �����������: ������ ������ ���������� ����� ��
1 �� ������, ��� ������� ������ ��������� ������� (h[i] = (h[i-1] + h[i+1])/2 - 1  ��� 1 < i < N). ���������
����� ����������� ������ ������� ����� B (B = hn) ��� �������, ��� ���� ���� �������� �����
�������� �����, � ��� ��������� ����������� ������� h[i] > 0.
���������: ����������� �������� �����.
������ �������� �����
� ������ ������ �������� ����� ���������� ��� ����� n � A (3 <= n <= 1000, n � �����, 10 <= A <= 1000, A � ������������).
������ ��������� �����
������� ���� ������������ ����� B � ����� ������� ����� �������.
������ 1
garland.in 
8 15 
garland.out
9.75
������ 2
garland.in 
692 532.81 
garland.out
446113.34
*/

#include <iostream>
#include <iomanip>
#include <fstream>
 
using namespace std;
 
int main()
{
    ifstream in ("garland.in");
    ofstream out ("garland.out");
    
    int n;
    double A;
    
    in >> n >> A;
    
    double   left = 0 , right = A ,current , next, middle , previous , last = -1;      
            
    bool above;
    while((right - left) > 0.01 / (n - 1)) {
        middle = (left + right) / 2;
        previous = A;
        current = middle;
        above = current > 0;
        for(int i = 3 ; i <= n ; i++) {
            next = 2 * current - previous + 2;
            above &= next > 0;
            previous = current;
            current = next;
        }
        if(above) {
            right = middle;
            last = current;
        }
        else {
            left = middle;
        }
    }
    out << setprecision(2) << fixed << last;

    return 0;
}
