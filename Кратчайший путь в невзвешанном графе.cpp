/*���������� ���� � ������������ ����� 
��� �������� �����: pathbge1.in
��� ��������� �����: pathbge1.out
����������� �� �������: 2 �������
����������� �� ������: 64 ���������
��� ����������������� ������������ ����. ������� ���������� ���������� �� ������ �������
�� ���� ������.
������ �������� �����
� ������ ������ �������� ����� ��� �����: n � m (2 <= n <= 30000, 1 <= m <= 400000), ��� n � ���������� ������ �����, � m � ���������� �����.
��������� m ����� �������� �������� �����. ������ ����� �������� ��������� �������� �
�������� ��������. ������� ���������� � �������.
������ ��������� �����
�������� n ����� � ��� ������ ������� ���������� ���������� �� ���.
������
pathbge1.in 
2 1
2 1
pathbge1.out
0 1
*/

#include <fstream>
#include <iostream>
#include <vector>
#include <queue>

using namespace std;

vector < vector <int> > g;

int main(){
	
	ifstream in ("pathbge1.in");
    ofstream out ("pathbge1.out");
	
	int n,m,x,y,i,j;
	
	queue <int> q;
	q.push(0);
	
	in >> n >> m;
	int rast[n];
	
	bool used[n];
	used[0] = true;
	
	g.resize(n);
	
	for(i = 0 ; i < n ; i++){
		rast[i] = 0;
		used[i] = false;
	}
	
	for(i = 0 ; i < m ; i++){
		in >> x >> y;
		g[x-1].push_back(y-1);
		g[y-1].push_back(x-1);
	}
	

	
	while (!q.empty()){
		int v = q.front();
		q.pop();
		for (i = 0 ; i < g[v].size() ; i++){
			int next = g[v][i];
			if (used[next] == false){
				used[next] = true;
				q.push(next);
				rast [next] = rast[v] + 1;
			}
			
		}
	}
	
	out << "0 ";
	for(i = 1 ; i < n ; i++){
		out <<  rast[i] << " ";
	}
	
	return 0;	
}
