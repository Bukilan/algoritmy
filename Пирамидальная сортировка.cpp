/*������������� ���������� 
��� �������� �����: sort.in
��� ��������� �����: sort.out
����������� �� �������: 2 �������
����������� �� ������: 256 ��������
��� ������ ����� �����. ���� ������ � ������������� ��� � ������� ���������� � �������
������������� ���������� (heap sort). �� �������, ���������� �� ����� ������ �����������,
����� ��������� �� �����.
������ �������� �����
� ������ ������ �������� ����� ���������� ����� n (1 <= n <= 100000) � ���������� ��������� � �������. 
�� ������ ������ ��������� n ����� �����, �� ������ �� ������������� 10^9
������ ��������� �����
� �������� ���� ���� ������� ���� �� ������ � ������� ����������, ����� ������ �����
������� ������ ������ ����� ���� ������.
������
sort.in 
10
1 8 2 1 4 7 3 2 3 6
sort.out
1 1 2 2 3 3 4 6 7 8
*/

#include <iostream>
#include <fstream>

using namespace std;

void siftDown(int *A , int i , int n) {
   
    int elem = A[i];
    int child;
    while(i <= n >> 1) {
        child = i << 1;
        if(child < n && A[child] < A[child + 1]) {
            child++;
        }
        if(elem >= A[child]) {
            break;
        }
        A[i] = A[child];
        i = child;
    }
    A[i] = elem;
}

void heapsort(int *A , int n) {
    for(int i = (n >> 1) - 1 ; i >= 0 ; i--) {
        siftDown(A , i , n - 1);
    }
    for(int i = n - 1 ; i >= 1 ; i--) {
        swap(A[0] , A[i]);
        siftDown(A , 0 , i - 1);
    }
}

int main() {
    ifstream cin ("sort.in");
    ofstream cout ("sort.out");

    int n;
    cin >> n;
    int *A = new int[n];
    for(int i = 0 ; i < n ; i++) {
        cin >> A[i];
    }
    heapsort(A , n);
    for(int i = 0 ; i < n ; i++) {
        cout << A[i] << " ";
    }

}
