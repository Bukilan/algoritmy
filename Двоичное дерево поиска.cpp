/*������� �������� ������ ������ (2 �����)
��� �������� �����: bstsimple.in
��� ��������� �����: bstsimple.out
����������� �� �������: 2 �������
����������� �� ������: 256 ��������
���������� �������� ������ ������.
������ �������� �����
������� ���� �������� �������� �������� � �������, �� ���������� �� ��������� 100. � ������
������ ��������� ���� �� ��������� ��������:
� insert x � �������� � ������ ���� x
� delete x � ������� �� ������ ���� x. ���� ����� x � ������ ���, �� ������ ������ �� ����
� exists x � ���� ���� x ���� � ������ �������� �true�, ���� ��� �false�
� next x � �������� ����������� ������� � ������, ������ ������� x, ��� �none� ���� ������ ���
� prev x � �������� ������������ ������� � ������, ������ ������� x, ��� �none� ���� ������
���
� ������ ���������� � ����������� ������ ����� �����, �� ����������� �� ������ 10^9
������ ��������� �����
�������� ��������������� ��������� ���������� ���� �������� exists, next, prev. ��������
������� ��������� ����� �� �������.
������
bstsimple.in 
insert 2
insert 5
insert 3
exists 2
exists 4
next 4
prev 4
delete 5
next 4
prev 4
bstsimple.out
true
false
5
3
none
3
*/

#include <iostream>
#include <fstream>
 
using namespace std;
 
struct Node
{
    int key;
    Node *left = NULL;
    Node *right = NULL;
    Node *parent = NULL;
};
 
class Binary_Tree
{
    private:
        Node *root;
    public:
        Binary_Tree()
        {
            root = NULL;
        }
        void * set_root(Node *x){
    		this -> root = x;
		}
		
        Node * get_root(){
    		return this -> root;
		}
		
        Node * minimum(Node *x){
    		return x -> left ? minimum(x -> left) : x;
		}
		
        Node * maximum(Node *x){
   			return x -> right ? maximum(x -> right) : x;
		}
		
        Node * next(int x){
		    Node *current = this -> root , *possible = NULL;
		    while(current != NULL){
		        if(current -> key > x){
		            possible = current;
		            current = current -> left;
		        }
		        else{
		            current = current -> right;
		        }
	    	}
	    return possible;
		}
        Node * prev(int x){
		    Node *current = this -> root , *possible = NULL;
		    while(current != NULL){
		        if(current -> key < x) {
		            possible = current;
		            current = current -> right;
		        }
		        else{
		            current = current -> left;
		        }
		    }
		    return possible;
		}
        Node * exists(int x){
		    Node *current = this -> root;
		    while(current != NULL && current -> key != x)
		    {
		        current = current -> key > x ? current -> left : current -> right;
		    }
		    return current;
		}
        Node * element_insert(Node *x , int z){
		    if(x == NULL){
		        x = new Node;
		        x -> parent = NULL;
		        x -> left = NULL;
		        x -> right = NULL;
		        x -> key = z;
		        return x;
		    }
		    else{
		        if(z < x -> key){
		            x -> left = element_insert(x -> left , z);
		        }
		        if(z > x -> key){
		            x -> right = element_insert(x -> right , z);
		        }
		    }
		    return x;
		}
 
        Node * element_delete(Node *x , int z){
		    if(x == NULL){
		        return x;
		    }
		    if(z < x -> key){
		        x -> left = element_delete(x -> left , z);
		    }
		    else{
		        if(z > x -> key) {
		            x -> right = element_delete(x -> right , z);
		        }
		        else{
		            if(x -> left != NULL && x -> right != NULL){
		                x -> key = minimum(x -> right) -> key;
		                x -> right = element_delete(x -> right , x -> key);
		            }
		            else {
		                if(x -> left != NULL) {
		                    x = x -> left;
		                }
		                else{
		                    x = x -> right;
		                }
		            }
		        }
		    }
		    return x;
		}
        void tree_delete(Node *leaf){
		    if(leaf != NULL)
		    {
		        tree_delete(leaf -> left);
		        tree_delete(leaf -> right);
		        delete leaf;
		    }
		}
        ~Binary_Tree()
        {
            tree_delete(root);
        }
};
 

 

 
int main()
{
    ifstream in ("bstsimple.in");
    ofstream out ("bstsimple.out");

	Binary_Tree A;
    Node *result = new Node;
    string str;
	int x;
    
    while(in >> str)
    {
        in >> x;
        if(str == "insert")
        {
            A.set_root(A.element_insert(A.get_root() , x));
        }
        if(str == "delete")
        {
            A.set_root(A.element_delete(A.get_root() , x));
        }
        if(str == "next")
        {
            result = A.next(x);
            if(result == NULL)
            {
                out << "none" << endl;
            }
            else
            {
                out << result -> key << endl;
            }
        }
        if(str == "prev")
        {
            result = A.prev(x);
            if(result == NULL)
            {
                out << "none" << endl;
            }
            else
            {
                out << result -> key << endl;
            }
        }
        if(str == "exists")
        {
            result = A.exists(x);
            out << (result ? "true" : "false") << endl;
        }
    }

    return 0;
}
