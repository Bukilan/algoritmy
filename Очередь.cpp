/*
������� 
��� �������� �����: queue.in
��� ��������� �����: queue.out
����������� �� �������: 2 �������
����������� �� ������: 64 ���������
���������� ������ �������. ��� ������ �������� ������� �������� �������� �� ���������.
�� ���� ��������� �������� ������, ���������� �������. ������ ������ �������� ���� �������.
������� � ��� ���� �+ N�, ���� �-�. ������� �+ N� �������� ���������� � ������� ����� N,
�� ������ �� ������������ 10^9
. ������� �-� �������� ������� �������� �� �������. �������������,
��� ������ ������� � �������� ���������� ������ �� �������� 106
���������.
������ �������� �����
� ������ ������ ���������� ���������� ������ � M (1 <= M <= 10^6). 
� ����������� ������� ���������� �������, �� ����� � ������ ������.
������ ��������� �����
�������� �����, ������� ��������� �� �������, �� ������ � ������ ������. �������������, ���
���������� �� ������ ������� �� ������������.
������
queue.in 
4
+ 1
+ 10
-
-
queue.out
1
10
*/

#include <iostream>
#include <fstream>

using namespace std;

class Queue {

private:
    struct Info {
        long long x;
        Info *prev = NULL;
        Info *next = NULL;
    };
    Info *head;
    Info *tail;

public:
    Queue() {
        head = NULL;
        tail = NULL;
    }

    void push(long long x);
    long long top();
    bool isEmpty();
    void pop();

    ~Queue() {
        while(!isEmpty()) {
            Info *p = tail;
            tail = tail -> next;
            delete p;
        }
    }
};

void Queue:: push(long long x) {
    Info *p = new Info;
    p -> next = NULL;
    p -> x = x;
    if(tail != NULL) {
        p -> prev = head;
        head -> next = p;
        head = p;
    }
    else {
        p -> prev = NULL;
        head = p;
        tail = p;
    }
}

long long Queue:: top() {
    return tail -> x;
}

bool Queue:: isEmpty() {
    return tail ? false : true;
}

void Queue:: pop() {
    if(!isEmpty()) {
        Info *p = tail;
        tail = tail -> next;
        delete p;
    }
}

int main()
{
    ifstream cin("queue.in");
    ofstream cout ("queue.out");

    Queue A;
    int n;
    cin >> n;
    char c;
    long long x;
    for(int i = 0 ; i < n ; i++) {
        cin >> c;
        if(c == '+') {
            cin >> x;
            A.push(x);
        }
        if(c == '-') {
            cout << A.top() << endl;
            A.pop();
        }
    }
    return 0;
}
