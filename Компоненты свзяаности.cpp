/*
 ���������� ��������� 
��� �������� �����: components.in
��� ��������� �����: components.out
����������� �� �������: 2 �������
����������� �� ������: 256 ��������
��� ����������������� ����. ��������� �������� ���������� ��������� � ���. ���������: ���
������� ������ ����� ��������������� ������� � ������ ��� ������� � �������.
������ �������� �����
������ ������ �������� ����� �������� ��� ����������� ����� n � m � ���������� ������ �
����� ����� �������������� (1 <= n <= 100 000, 0 <= m <= 200 000).
��������� m ����� �������� �������� ����� �� ������ �� ������. ����� ����� i �����������
����� ������������ ������� b[i] , e[i] � �������� ������ ����� (1 <= b[i] , e[i] <= n). ����������� ����� � ������������ �����.
������ ��������� �����
� ������ ������ ��������� ����� �������� ����� ����� k � ���������� ��������� ���������
�����. �� ������ ������ �������� n ����������� ����� a1, a1, . . . , an, �� ������������� k, ��� ai �
����� ���������� ���������, ������� ����������� i-� �������.
������
components.in 
3 1
1 2
components.out
2
1 1 2
*/

#include <fstream>
#include <iostream>
#include <vector>

using namespace std;

vector < vector<int> > g;
vector <int> comp;
bool *used;
int count = 0;

void dfs (int u){
	used[u] = true;
	comp[u] = count;
	for(int i = 0 ; i < g[u].size() ; i++){
		if(used[g[u][i]] == false){
			dfs (g[u][i]);
		}
	}
}

int main(){
	
	ifstream in ("components.in");
    ofstream out ("components.out");
	
	int n,m,x,y,i,j;
	
	in >> n >> m;
	g.resize(n);
	comp.resize(n, 0);
	used = new bool [n];
	
	for (i = 0 ; i < n ; i++){
		used[i] = false;
	}
	for (i = 0 ; i < m ; i++){
		in >> x >> y;
		g[x-1].push_back(y-1);
		g[y-1].push_back(x-1);
	}
	for ( i = 0 ; i < n ; i++){
		if(used[i] == false){
			count++;
			dfs(i);
		}
	}
	
	out << count << "\n";
	for ( i = 0 ; i < comp.size() ; i++){
		out << comp[i] << " ";
	}
	return 0;
}
