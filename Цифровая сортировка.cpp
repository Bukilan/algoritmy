/*
�������� ���������� 
��� �������� �����: radixsort.in
��� ��������� �����: radixsort.out
����������� �� �������: 2 �������
����������� �� ������: 256 ��������
���� n �����, �������� �� ������� ����� k ��� �������� ����������.
������ �������� �����
� ������ ������ �������� ����� ���������� ����� n � ���������� �����, m � �� ����� � k �
����� ��� �������� ���������� (1 <= n <= 1000, 1 <= k <= m <= 1000). 
� ��������� n ������� ��������� ���� ������.
������ ��������� �����
�������� ������ � ������� � ������� ��� ����� ����� k ��� �������� ����������.
������ 1
radixsort.in 
3 3 1
bbb
aba
baa
radixsort.out
aba
baa
bbb
������ 2
radixsort.in 
3 3 2
bbb
aba
baa
radixsort.out
baa
aba
bbb
������ 3
radixsort.in 
3 3 3
bbb
aba
baa
radixsort.out
aba
baa
bbb
*/

#include <iostream>
#include <fstream>

using namespace std;

int n , m;

char ot(string s , int i)
{
    return s[i];
}

void radix(string *a , int k)
{
    int C[m];
    string B[n];

    for(int i = 0 ; i < m ; i++){
        C[i] = 0;
    }
    for(int i = 0 ; i < n ; i++){
        C[ot(a[i] , k) - 'a']++;
    }
    for(int i = 1 ; i < m ; i++){
        C[i] += C[i - 1];
    }
    for(int i = n - 1 ; i >= 0 ; i--){
        B[C[ot(a[i] , k) - 'a'] - 1] = a[i];
        C[ot(a[i] , k) - 'a']--;
    }
    for(int i = 0 ; i < n ; i++){
        a[i] = B[i];
    }
}

int main()
{
    ifstream fin ("radixsort.in");
    ofstream fout ("radixsort.out");

    int k;
    fin >> n >> m >> k;

    string str[n];
    for(int i = 0 ; i < n ; i++){
        fin >> str[i];
    }

    for(int i = m - 1 ; i > m - 1 - k ; i--){
        radix(str , i);
    }

    for(int i = 0 ; i < n ; i++){
        fout << str[i]<< endl;
    }

    return 0;
}
