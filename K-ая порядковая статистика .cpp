/*K-�� ���������� ���������� 
��� �������� �����: kth.in
��� ��������� �����: kth.out
����������� �� �������: 2 �������
����������� �� ������: 256 ��������
��� ������ �� n ���������. ����� ����� k-�� � ������� ����������� � ���� �������?
������ �������� �����
� ������ ������ �������� ����� ���������� ��� ����� n � ������ ������� �
k. (1 <= k <= n <= 3 � 10^7)
�� ������ ������ ��������� ����� A, B, C, a1, a2 �� ������ ��
������������� 10^9
�� ������ �������� �������� ������� ������� � �������� �� �������:
a[i] = A * a[i-2] + B * a[i-1] + C. ��� ���������� ������ ������������ � 32 ������ �������� ����,
������������ ������ ��������������.
������ ��������� �����
�������� k-�� � ������� ����������� ����� � ������� a.
������ 1
kth.in 
5 3
2 3 5 1 2
kth.out
13
������ 2
kth.in 
5 3
200000 300000 5 1 2
kth.out
2
�� ������ ������� �������� ������� a �����: (1, 2, 800005, ?516268571, 1331571109).
*/
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>

using namespace std;

int stat(int *A , int n , int k)
{
    int left, right,mid, i, j, median;
    left = 0;
    right = n;
    while(true)
    {
        if(left + 1 >= right)
        {
            if(right == left + 1 && A[left] > A[right])
            {
                swap(A[left] , A[right]);
            }
            return A[k];
        }
        mid = (left + right) >> 1;
        swap(A[mid] , A[left + 1]);
        if(A[left] > A[right])
        {
            swap(A[left] , A[right]);
        }
        if(A[left + 1] > A[right])
        {
            swap(A[left + 1] , A[right]);
        }
        if(A[left] > A[left + 1])
        {
            swap(A[left] , A[left + 1]);
        }
        i = left + 1;
        j = right;
        median = A[left + 1];
        while(true)
        {
            while(A[++i] < median);
            while(A[--j] > median);
            if(i > j)
            {
                break;
            }
            swap(A[i] , A[j]);
        }
        A[left + 1] = A[j];
        A[j] = median;
        if(j >= k)
        {
            right = j - 1;
        }
        if(j <= k)
        {
            left = i;
        }
    }
}

int main()
{
    ifstream fin ("kth.in");
    ofstream fout ("kth.out");
    
    int n , k;
    fin >> n >> k;
    int A , B , C , a[n];
    fin >> A >> B >> C >> a[0] >> a[1];
    for(int i = 2 ; i < n ; i++)
    {
        a[i] = A * a[i - 2] + B * a[i - 1] + C;
    }
    fout << stat(a , n - 1 , k - 1);

    return 0;
}


